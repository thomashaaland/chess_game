#include "../headerfiles/save_handler.h"

// Default Constructor
SaveHandler::SaveHandler() {}

// Default Destructor;
SaveHandler::~SaveHandler() {}

/**
 * @brief Method which saves a game state to save game buffer in memory.
 * It takes a board by reference and a save slot and saves the state
 * to memory.
 * @param board_to_save
 * @param save_slot
 **/
void SaveHandler::save_board_state(Board &board_to_save, int save_slot)
{
    auto board = board_to_save.getBoard();
    std::vector<std::pair<std::string, std::pair<int, int>>> pieces;
    for (Piece *piece_ptr : board)
    {
        if (piece_ptr != nullptr)
        {
            auto info = std::make_pair(piece_ptr->get_symbol(), piece_ptr->get_position());
            pieces.push_back(info);
        }
    }
    // savegames[save_slot] = pieces;
    savegames_w_history[save_slot].push_back(pieces);
}

/**
 * @brief Method which saves data to disk. It needs a filename
 * to write to.
 *
 * @param filename
 */
void SaveHandler::write_savegames_to_file(std::string filename)
{
    std::ofstream data_file;
    data_file.open(filename);
    if (data_file.is_open())
    {
        hps::to_stream(savegames_w_history, data_file);
    }
    else
    {
        std::cout << "Failure to open file" << std::endl;
    }
    data_file.close();
}

/**
 * @brief Loads the state of a board. It needs a save slot and
 * returns a pointer to a board. It build a new board if the save
 * is empty.
 *
 * @param save_location
 * @return Board*
 */
Board *SaveHandler::load_board_state(int save_location, int frame)
{
    Board *new_board = new Board();
    auto saveslot = savegames_w_history[save_location];
    if (saveslot.empty())
    {
        new_board->initialize_board();
    }
    else if (frame == -1)
    {
        new_board->reconstruct_board(saveslot.back());
    }
    else
    {
        new_board->reconstruct_board(saveslot[frame]);
    }
    return new_board;
}

/**
 * @brief Method which delets a save slot in the slot save_slot.
 *
 * @param save_slot
 */
void SaveHandler::delete_save_slot(int save_slot)
{
    savegames_w_history[save_slot].clear();
}

/**
 * @brief Method to load saves from file
 *
 * @param filename
 */
void SaveHandler::load_savegames_from_file(std::string filename)
{
    std::vector<std::pair<std::string, std::pair<int, int>>> pieces;
    std::string line;
    std::ifstream data_file;
    data_file.open(filename);
    if (data_file.is_open())
    {
        savegames_w_history = hps::from_stream<std::array<
            std::vector<std::vector<std::pair<std::string, std::pair<int, int>>>>, 10>>(data_file);
    }
    else
    {
        std::cout << "Could not open file " << filename << std::endl;
    }
    data_file.close();
}

/**
 * @brief prints all saves as a list
 *
 */
void SaveHandler::print_saves()
{
    int count = 0;
    std::cout << "### SAVES ###" << std::endl;
    for (auto save : savegames_w_history)
    {
        std::cout << " " << count++;
        std::cout << (save.empty() ? " --- EMPTY --- " : " --- NOT EMPTY --- ");
        std::cout << std::endl;
    }
}

std::vector<int> SaveHandler::get_valid_save_slots()
{
    std::vector<int> valid_saves;
    int count = 0;
    for (auto save : savegames_w_history)
    {
        if (!save.empty())
            valid_saves.push_back(count);
        count++;
    }
    return valid_saves;
}

#include <iostream>
#include "../headerfiles/board.h"
#include "../headerfiles/board_printer.h"
#include "../headerfiles/game_engine.h"
#include "../headerfiles/inputhandler.h"
#include "../headerfiles/piece.h"
#include "../headerfiles/player.h"

int main()
{
	int mode = 0;
	std::cout << "Select mode:\n 1: Play Chess!\n 2: watch a replay\n > ";
	std::cin >> mode;
	switch (mode)
	{
	case 1:
	{
		GameEngine game;
		bool b_game_over = false;
		game.init();
		while (!b_game_over)
		{
			b_game_over = game.game_loop();
		}
		break;
	}
	case 2:
	{
		GameEngine game;
		while (game.replay()) {}
	}
	break;
	case 4:
	{
	}
	break;
	}
	return 0;
}

#include "../headerfiles/piece.h"
#include "../headerfiles/board.h"
#include <iostream>
#include <gtest/gtest.h>

// Definitions of boards for GTest-cases

Board empty_board()
{
    Board board;
    for (int x = 0; x < 8; x++)
    {
        for (int y = 0; y < 8; y++)
        {
            board.add_nullptr_to_board({x, y});
        }
    }
    return board;
}

Board king_meets_pawn()
{
    Board board;
    Piece *my_piece = new King(std::make_pair<int, int>(3, 1), black);
    board.add_piece_to_board(std::make_pair<int, int>(3, 1), my_piece);
    Piece *my_piece2 = new Pawn(std::make_pair<int, int>(7, 5), white);
    board.add_piece_to_board(std::make_pair<int, int>(7, 5), my_piece2);

    for (int x = 0; x < 8; x++)
    {
        for (int y = 0; y < 8; y++)
        {
            if (((x == 3) && (y == 1)) || ((x == 7) && (y == 5)))
            {
                continue;
            }
            board.add_nullptr_to_board({x, y});
        }
    }
    return board;
}

Board L_pawns()
{
    Board board;
    Piece *my_piece = new Pawn(std::make_pair<int, int>(2, 4), white);
    board.add_piece_to_board(std::make_pair<int, int>(2, 4), my_piece);
    Piece *my_piece2 = new Pawn(std::make_pair<int, int>(2, 5), white);
    board.add_piece_to_board(std::make_pair<int, int>(2, 5), my_piece2);
    Piece *my_piece3 = new Pawn(std::make_pair<int, int>(3, 5), black);
    board.add_piece_to_board(std::make_pair<int, int>(3, 5), my_piece3);

    for (int x = 0; x < 8; x++)
    {
        for (int y = 0; y < 8; y++)
        {
            if (((x == 2) && (y == 4)) || ((x == 2) && (y == 5)) || ((x == 3) && (y == 5)))
            {
                continue;
            }
            board.add_nullptr_to_board({x, y});
        }
    }
    return board;
}

Board L_black_pawns()
{
    Board board;
    Piece *my_piece = new Pawn(std::make_pair<int, int>(2, 4), black);
    board.add_piece_to_board(std::make_pair<int, int>(2, 4), my_piece);
    Piece *my_piece2 = new Pawn(std::make_pair<int, int>(2, 5), black);
    board.add_piece_to_board(std::make_pair<int, int>(2, 5), my_piece2);
    Piece *my_piece3 = new Pawn(std::make_pair<int, int>(3, 5), black);
    board.add_piece_to_board(std::make_pair<int, int>(3, 5), my_piece3);

    for (int x = 0; x < 8; x++)
    {
        for (int y = 0; y < 8; y++)
        {
            if (((x == 2) && (y == 4)) || ((x == 2) && (y == 5)) || ((x == 3) && (y == 5)))
            {
                continue;
            }
            board.add_nullptr_to_board({x, y});
        }
    }
    return board;
}

Board cross_chess()
{
    Board board;
    Piece *my_piece = new Pawn(std::make_pair<int, int>(2, 4), white);
    board.add_piece_to_board(std::make_pair<int, int>(2, 4), my_piece);
    Piece *my_piece2 = new Pawn(std::make_pair<int, int>(4, 4), white);
    board.add_piece_to_board(std::make_pair<int, int>(4, 4), my_piece2);
    Piece *my_piece3 = new Pawn(std::make_pair<int, int>(2, 6), white);
    board.add_piece_to_board(std::make_pair<int, int>(2, 6), my_piece3);
    Piece *my_piece4 = new Pawn(std::make_pair<int, int>(4, 6), white);
    board.add_piece_to_board(std::make_pair<int, int>(4, 6), my_piece4);

    for (int x = 0; x < 8; x++)
    {
        for (int y = 0; y < 8; y++)
        {
            if (((x == 2) && (y == 4)) || ((x == 4) && (y == 4)) || ((x == 2) && (y == 6)) || ((x == 4) && (y == 6)))
            {
                continue;
            }
            board.add_nullptr_to_board({x, y});
        }
    }
    return board;
}

// Testing begins here

// Tests rook pieces
TEST(Singlerookswhite1, get_legal_moves)
{
    Board board = empty_board();
    Rook *rook_ptr;
    rook_ptr = new Rook(std::make_pair<int, int>(0, 0), color_t::white);

    auto legal_moves = rook_ptr->get_legal_moves(board.getBoard());
    std::vector<std::pair<int, int>> correct_moves;
    correct_moves = {std::make_pair<int, int>(0, 1), std::make_pair<int, int>(0, 2), std::make_pair<int, int>(0, 3),
                     std::make_pair<int, int>(0, 4), std::make_pair<int, int>(0, 5), std::make_pair<int, int>(0, 6),
                     std::make_pair<int, int>(0, 7), std::make_pair<int, int>(1, 0), std::make_pair<int, int>(2, 0),
                     std::make_pair<int, int>(3, 0), std::make_pair<int, int>(4, 0), std::make_pair<int, int>(5, 0),
                     std::make_pair<int, int>(6, 0), std::make_pair<int, int>(7, 0)};
    for (size_t i = 0; i < correct_moves.size(); i++)
    {
        ASSERT_EQ(true, find(legal_moves.begin(), legal_moves.end(), correct_moves[i]) != legal_moves.end());
    }
    delete rook_ptr;
}

TEST(Singlerookswhite2, get_legal_moves)
{
    Board board = empty_board();
    Rook *rook_ptr;
    rook_ptr = new Rook(std::make_pair<int, int>(7, 0), color_t::white);

    auto legal_moves = rook_ptr->get_legal_moves(board.getBoard());
    std::vector<std::pair<int, int>> correct_moves;
    correct_moves = {std::make_pair<int, int>(0, 0), std::make_pair<int, int>(1, 0), std::make_pair<int, int>(2, 0),
                     std::make_pair<int, int>(3, 0), std::make_pair<int, int>(4, 0), std::make_pair<int, int>(5, 0),
                     std::make_pair<int, int>(6, 0), std::make_pair<int, int>(7, 1), std::make_pair<int, int>(7, 2),
                     std::make_pair<int, int>(7, 3), std::make_pair<int, int>(7, 4), std::make_pair<int, int>(7, 5),
                     std::make_pair<int, int>(7, 6), std::make_pair<int, int>(7, 7)};
    for (size_t i = 0; i < correct_moves.size(); i++)
    {
        ASSERT_EQ(true, find(legal_moves.begin(), legal_moves.end(), correct_moves[i]) != legal_moves.end());
    }
    delete rook_ptr;
}

TEST(Singlerooksblack1, get_legal_moves)
{
    Board board = empty_board();
    Rook *rook_ptr;
    rook_ptr = new Rook(std::make_pair<int, int>(7, 0), color_t::black);

    auto legal_moves = rook_ptr->get_legal_moves(board.getBoard());
    std::vector<std::pair<int, int>> correct_moves;
    correct_moves = {std::make_pair<int, int>(0, 0), std::make_pair<int, int>(1, 0), std::make_pair<int, int>(2, 0),
                     std::make_pair<int, int>(3, 0), std::make_pair<int, int>(4, 0), std::make_pair<int, int>(5, 0),
                     std::make_pair<int, int>(6, 0), std::make_pair<int, int>(7, 1), std::make_pair<int, int>(7, 2),
                     std::make_pair<int, int>(7, 3), std::make_pair<int, int>(7, 4), std::make_pair<int, int>(7, 5),
                     std::make_pair<int, int>(7, 6), std::make_pair<int, int>(7, 7)};
    for (size_t i = 0; i < correct_moves.size(); i++)
    {
        ASSERT_EQ(true, find(legal_moves.begin(), legal_moves.end(), correct_moves[i]) != legal_moves.end());
    }
    delete rook_ptr;
}

TEST(Singlerooksblack2, get_legal_moves)
{
    Board board = empty_board();
    Rook *rook_ptr;
    rook_ptr = new Rook(std::make_pair<int, int>(7, 7), color_t::black);

    auto legal_moves = rook_ptr->get_legal_moves(board.getBoard());
    std::vector<std::pair<int, int>> correct_moves;
    correct_moves = {std::make_pair<int, int>(0, 7), std::make_pair<int, int>(1, 7), std::make_pair<int, int>(2, 7),
                     std::make_pair<int, int>(3, 7), std::make_pair<int, int>(4, 7), std::make_pair<int, int>(5, 7),
                     std::make_pair<int, int>(6, 7), std::make_pair<int, int>(7, 0), std::make_pair<int, int>(7, 1),
                     std::make_pair<int, int>(7, 2), std::make_pair<int, int>(7, 3), std::make_pair<int, int>(7, 4),
                     std::make_pair<int, int>(7, 5), std::make_pair<int, int>(7, 6)};
    for (size_t i = 0; i < correct_moves.size(); i++)
    {
        ASSERT_EQ(true, find(legal_moves.begin(), legal_moves.end(), correct_moves[i]) != legal_moves.end());
    }
    delete rook_ptr;
}

TEST(Rook_meets_king_pawn, get_legal_moves)
{
    Board board = king_meets_pawn();
    Rook *rook_ptr;
    rook_ptr = new Rook(std::make_pair<int, int>(1, 1), color_t::white);

    auto legal_moves = rook_ptr->get_legal_moves(board.getBoard());
    std::vector<std::pair<int, int>> correct_moves;
    correct_moves = {std::make_pair<int, int>(2, 1), std::make_pair<int, int>(3, 1), std::make_pair<int, int>(0, 1),
                     std::make_pair<int, int>(1, 0), std::make_pair<int, int>(1, 2), std::make_pair<int, int>(1, 3),
                     std::make_pair<int, int>(1, 4), std::make_pair<int, int>(1, 5), std::make_pair<int, int>(1, 6),
                     std::make_pair<int, int>(1, 7)};
    for (size_t i = 0; i < correct_moves.size(); i++)
    {
        ASSERT_EQ(true, find(legal_moves.begin(), legal_moves.end(), correct_moves[i]) != legal_moves.end());
    }

    delete rook_ptr;
}

// Test Bishop Pieces
TEST(SingleBishop, get_legal_moves)
{
    Board board = empty_board();
    Bishop *bishop_ptr;
    bishop_ptr = new Bishop(std::make_pair<int, int>(2, 0), color_t::white);

    auto legal_moves = bishop_ptr->get_legal_moves(board.getBoard());
    std::vector<std::pair<int, int>> correct_moves;
    correct_moves = {std::make_pair<int, int>(3, 1), std::make_pair<int, int>(4, 2), std::make_pair<int, int>(5, 3),
                     std::make_pair<int, int>(6, 4), std::make_pair<int, int>(7, 5), std::make_pair<int, int>(1, 1),
                     std::make_pair<int, int>(0, 2)};
    for (size_t i = 0; i < correct_moves.size(); i++)
    {
        ASSERT_EQ(true, find(legal_moves.begin(), legal_moves.end(), correct_moves[i]) != legal_moves.end());
    }
    delete bishop_ptr;
}

TEST(Bishop_meets_king_pawn, get_legal_moves)
{
    Board board = king_meets_pawn();
    Bishop *bishop_ptr;
    bishop_ptr = new Bishop(std::make_pair<int, int>(5, 3), color_t::white);

    auto legal_moves = bishop_ptr->get_legal_moves(board.getBoard());
    std::vector<std::pair<int, int>> correct_moves;
    correct_moves = {std::make_pair<int, int>(6, 4), std::make_pair<int, int>(4, 2), std::make_pair<int, int>(3, 1),
                     std::make_pair<int, int>(6, 2), std::make_pair<int, int>(7, 1), std::make_pair<int, int>(4, 4),
                     std::make_pair<int, int>(3, 5), std::make_pair<int, int>(2, 6), std::make_pair<int, int>(1, 7)};
    for (size_t i = 0; i < correct_moves.size(); i++)
    {
        ASSERT_EQ(true, find(legal_moves.begin(), legal_moves.end(), correct_moves[i]) != legal_moves.end());
    }

    delete bishop_ptr;
}

// Test Queen Pieces

TEST(SingleQueen, get_legal_moves)
{
    Board board = empty_board();
    Queen *queen_ptr;
    queen_ptr = new Queen(std::make_pair<int, int>(3, 0), color_t::white);

    auto legal_moves = queen_ptr->get_legal_moves(board.getBoard());
    std::vector<std::pair<int, int>> correct_moves;
    correct_moves = {std::make_pair<int, int>(3, 1), std::make_pair<int, int>(3, 2), std::make_pair<int, int>(3, 3),
                     std::make_pair<int, int>(3, 4), std::make_pair<int, int>(3, 5), std::make_pair<int, int>(3, 6),
                     std::make_pair<int, int>(3, 7), std::make_pair<int, int>(2, 0), std::make_pair<int, int>(1, 0),
                     std::make_pair<int, int>(0, 0), std::make_pair<int, int>(4, 0), std::make_pair<int, int>(5, 0),
                     std::make_pair<int, int>(6, 0), std::make_pair<int, int>(7, 0), std::make_pair<int, int>(2, 1),
                     std::make_pair<int, int>(1, 2), std::make_pair<int, int>(0, 3), std::make_pair<int, int>(4, 1),
                     std::make_pair<int, int>(5, 2), std::make_pair<int, int>(6, 3), std::make_pair<int, int>(7, 4)};
    for (size_t i = 0; i < correct_moves.size(); i++)
    {
        ASSERT_EQ(true, find(legal_moves.begin(), legal_moves.end(), correct_moves[i]) != legal_moves.end());
    }
    delete queen_ptr;
}

// Test King Pieces

TEST(SingleKing, get_legal_moves)
{
    Board board = empty_board();
    King *king_ptr;
    king_ptr = new King(std::make_pair<int, int>(4, 0), color_t::white);

    auto legal_moves = king_ptr->get_legal_moves(board.getBoard());
    std::vector<std::pair<int, int>> correct_moves;
    correct_moves = {std::make_pair<int, int>(5, 0), std::make_pair<int, int>(3, 0), std::make_pair<int, int>(4, 1),
                     std::make_pair<int, int>(5, 1), std::make_pair<int, int>(3, 1)};
    for (size_t i = 0; i < correct_moves.size(); i++)
    {
        ASSERT_EQ(true, find(legal_moves.begin(), legal_moves.end(), correct_moves[i]) != legal_moves.end());
    }
    delete king_ptr;
}

// Test Knight Pieces

TEST(Singleknight, get_legal_moves)
{
    Board board = empty_board();
    Knight *knight_ptr;
    knight_ptr = new Knight(std::make_pair<int, int>(1, 0), color_t::white);

    auto legal_moves = knight_ptr->get_legal_moves(board.getBoard());
    std::vector<std::pair<int, int>> correct_moves;
    correct_moves = {std::make_pair<int, int>(0, 2), std::make_pair<int, int>(2, 2), std::make_pair<int, int>(3, 1)};
    for (size_t i = 0; i < correct_moves.size(); i++)
    {
        ASSERT_EQ(true, find(legal_moves.begin(), legal_moves.end(), correct_moves[i]) != legal_moves.end());
    }
    delete knight_ptr;
}

TEST(Cornerknight, get_legal_moves)
{
    Board board = empty_board();
    Knight *knight_ptr;
    knight_ptr = new Knight(std::make_pair<int, int>(0, 0), color_t::white);

    auto legal_moves = knight_ptr->get_legal_moves(board.getBoard());
    std::vector<std::pair<int, int>> correct_moves;
    correct_moves = {std::make_pair<int, int>(2, 1), std::make_pair<int, int>(1, 2)};
    for (size_t i = 0; i < correct_moves.size(); i++)
    {
        ASSERT_EQ(true, find(legal_moves.begin(), legal_moves.end(), correct_moves[i]) != legal_moves.end());
    }
    delete knight_ptr;
}

TEST(Jumpingknight, get_legal_moves)
{
    Board board = L_pawns();
    Knight *knight_ptr;
    knight_ptr = new Knight(std::make_pair<int, int>(2, 3), color_t::white);

    auto legal_moves = knight_ptr->get_legal_moves(board.getBoard());
    std::vector<std::pair<int, int>> correct_moves;
    correct_moves = {std::make_pair<int, int>(3, 5), std::make_pair<int, int>(1, 5), std::make_pair<int, int>(3, 1),
                     std::make_pair<int, int>(1, 1), std::make_pair<int, int>(4, 4), std::make_pair<int, int>(4, 2),
                     std::make_pair<int, int>(0, 4), std::make_pair<int, int>(0, 2)};
    for (size_t i = 0; i < correct_moves.size(); i++)
    {
        ASSERT_EQ(true, find(legal_moves.begin(), legal_moves.end(), correct_moves[i]) != legal_moves.end());
    }
    delete knight_ptr;
}

// Test Pawns Pieces

TEST(SinglePawn, get_legal_moves)
{
    Board board = empty_board();
    Pawn *pawn_ptr;
    pawn_ptr = new Pawn(std::make_pair<int, int>(1, 1), color_t::white);

    auto legal_moves = pawn_ptr->get_legal_moves(board.getBoard());
    std::vector<std::pair<int, int>> correct_moves;
    correct_moves = {std::make_pair<int, int>(1, 2), std::make_pair<int, int>(1, 3)};
    for (size_t i = 0; i < correct_moves.size(); i++)
    {
        ASSERT_EQ(true, find(legal_moves.begin(), legal_moves.end(), correct_moves[i]) != legal_moves.end());
    }
    delete pawn_ptr;
}
TEST(AttackingPawn, get_legal_moves)
{
    Board board = L_black_pawns();
    Pawn *pawn_ptr;
    pawn_ptr = new Pawn(std::make_pair<int, int>(3, 4), color_t::white);

    auto legal_moves = pawn_ptr->get_legal_moves(board.getBoard());
    std::vector<std::pair<int, int>> correct_moves;
    correct_moves = {std::make_pair<int, int>(2, 5)};
    for (size_t i = 0; i < correct_moves.size(); i++)
    {
        ASSERT_EQ(true, find(legal_moves.begin(), legal_moves.end(), correct_moves[i]) != legal_moves.end());
    }
    delete pawn_ptr;
}

TEST(Starpawns, get_legal_moves)
{
    Board board = cross_chess();
    Pawn *pawn_ptr;
    pawn_ptr = new Pawn(std::make_pair<int, int>(3, 5), color_t::black);

    auto legal_moves = pawn_ptr->get_legal_moves(board.getBoard());
    std::vector<std::pair<int, int>> correct_moves;
    correct_moves = {std::make_pair<int, int>(2, 4), std::make_pair<int, int>(3, 4), std::make_pair<int, int>(4, 4)};
    for (size_t i = 0; i < correct_moves.size(); i++)
    {
        ASSERT_EQ(true, find(legal_moves.begin(), legal_moves.end(), correct_moves[i]) != legal_moves.end());
    }
    delete pawn_ptr;
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
    return 0;
}

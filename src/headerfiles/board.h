#ifndef BOARD_H
#define BOARD_H

#include <array>
#include <string>
#include "piece.h"
#include "player.h"
#include "../../hps/src/hps.h"

using namespace std::string_literals;

// Class for chessboard.
class Board
{
private:
    std::array<Piece *, 64> board;
    std::array<Piece *, 32> trash_list;
    Piece *white_king;
    Piece *black_king;

public:
    Board();
    ~Board();
    int indice_function(std::pair<int, int>);
    void initialize_board();
    void reconstruct_board(std::vector<std::pair<std::string, std::pair<int, int>>>);
    Piece *getPiecePtr(std::pair<int, int>);
    bool move_piece(std::pair<int, int>, std::pair<int, int>, Player *);
    void add_piece_to_board(std::pair<int, int> coordinate, Piece *piece_ptr);
    void add_nullptr_to_board(std::pair<int,int>);

    std::array<Piece*, 64> getBoard();
    void winCondition();
    bool b_white_alive;
    bool b_black_alive;
};

#endif
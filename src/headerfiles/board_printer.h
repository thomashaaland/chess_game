#ifndef BOARD_PRINTER_H
#define BOARD_PRINTER_H
#include <algorithm>
#include "board.h"
#include "piece.h"

class BoardPrinter
{
private:
std::string color_s;

public:
    void print_board(Board &);
    void print_board_selected(Piece*, Board &);
    void set_player(color_t);
};

#endif
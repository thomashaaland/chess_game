#ifndef PIECE_H
#define PIECE_H

#include <iostream>
#include <vector>
#include <array>
#include <algorithm>

enum color_t
{
    white,
    black
};


// Base piece has the most basic functions.
class Piece
{
protected:
    std::pair<int, int> position;
    color_t color;
    std::string symbol;
    std::array<std::pair<int, int>, 4> cardinals;
    std::array<std::pair<int, int>, 4> diagonals;
    int inline ind(std::pair<int, int>);
    bool check_if_square_threatened(std::pair<int, int>, std::array<Piece *, 64>);
    void append_legal_moves_in_selected_direction(std::vector<std::pair<int, int>> &,
                                                  std::pair<int, int> &,
                                                  const std::array<Piece *, 64> &);
    void append_legal_move_in_selected_square(std::vector<std::pair<int, int>> &,
                                              std::pair<int, int> &,
                                              const std::array<Piece *, 64> &);

public:
    Piece(std::pair<int, int>, color_t);
    virtual ~Piece();
    virtual void move(std::pair<int, int>) = 0;
    virtual std::vector<std::pair<int, int>> get_legal_moves(const std::array<Piece *, 64> &) = 0;
    color_t get_color();
    std::string get_symbol();
    std::pair<int, int> get_position();
    int check_status_of_piece(Piece *);
    bool is_in_check(const std::array<Piece *, 64> &);
};

class Pawn : public Piece
{
public:
    Pawn(std::pair<int, int>, color_t);
    ~Pawn();
    void move(std::pair<int, int>) override;
    std::vector<std::pair<int, int>> get_legal_moves(const std::array<Piece *, 64> &) override;
};

class Queen : public Piece
{
public:
    Queen(std::pair<int, int>, color_t);
    ~Queen();
    void move(std::pair<int, int>) override;
    std::vector<std::pair<int, int>> get_legal_moves(const std::array<Piece *, 64> &) override;
};

class King : public Piece
{
public:
    King(std::pair<int, int>, color_t);
    ~King();
    void move(std::pair<int, int>) override;
    std::vector<std::pair<int, int>> get_legal_moves(const std::array<Piece *, 64> &) override;
};

class Rook : public Piece
{
public:
    Rook(std::pair<int, int>, color_t);
    ~Rook();
    void move(std::pair<int, int>) override;
    std::vector<std::pair<int, int>> get_legal_moves(const std::array<Piece *, 64> &) override;
};

class Knight : public Piece
{
public:
    Knight(std::pair<int, int>, color_t);
    ~Knight();
    void move(std::pair<int, int>) override;
    std::vector<std::pair<int, int>> get_legal_moves(const std::array<Piece *, 64> &) override;
};

class Bishop : public Piece
{
public:
    Bishop(std::pair<int, int>, color_t);
    ~Bishop();
    void move(std::pair<int, int>) override;
    std::vector<std::pair<int, int>> get_legal_moves(const std::array<Piece *, 64> &) override;
};

#endif
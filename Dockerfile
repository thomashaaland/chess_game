FROM gcc:11

RUN apt-get update --yes
RUN apt-get install --yes cmake
RUN apt-get install --yes libgtest-dev
RUN mkdir -p /usr/gtest

COPY . /usr/gtest/

RUN ls
RUN cd /usr/gtest; cmake CMakeLists.txt; make

ENTRYPOINT ["./usr/gtest/main"]
